package dam.android.vidal.u4t6contacts.model;

import android.net.Uri;

//TODO EX 10.1 Creem una clase Contact per a poder transferir les dades de cada contacte de manera individual
public class Contact {

    private String contactId;
    private String name;
    private String number;
    private String lookup;
    private String rawContact;
    private String type;
    private Uri photoThumbnail;

    public Contact(String contactId, String name, String number, String lookup, String rawContact
            , String type, Uri photoThumbnail) {
        this.contactId = contactId;
        this.name = name;
        this.number = number;
        this.lookup = lookup;
        this.rawContact = rawContact;
        this.type = type;
        this.photoThumbnail = photoThumbnail;
    }

    public String getContactId() {
        return contactId;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getLookup() {
        return lookup;
    }

    public String getRawContact() {
        return rawContact;
    }

    public String getType() {
        return type;
    }

    public Uri getPhotoThumbnail() {
        return photoThumbnail;
    }
}
