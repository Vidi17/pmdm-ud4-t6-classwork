package dam.android.vidal.u4t6contacts;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import dam.android.vidal.u4t6contacts.model.Contact;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnClickListener {
    MyContacts myContacts;
    RecyclerView recyclerView;

    //TODO EX 10.2 Instanciem el nou cardView Que acoplarem a la base de la activity
    CardView cardView;

    // Permissions required to contacts provider, only needed to READ
    private static final String[] PERMISSIONS_CONTACTS = {Manifest.permission.READ_CONTACTS};

    // Id to identify a contacts permission request
    private static final int REQUEST_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    //TODO EX 10.2 Em modificat el mètode onResume per a que revise els permisos i actualitze els contactes que tenim a la llista
    @Override
    protected void onResume() {
        if (checkPermissions()) {
            setListAdapter();
        }
        super.onResume();
    }

    private void setUI() {

        recyclerView = findViewById(R.id.recyclerViewContacts);

        //TODO EX 10.2 Ací enllacem el nou cardView amb la seua variable
        cardView = findViewById(R.id.baseCardView);
        recyclerView.setHasFixedSize(true);

        //TODO EX 10.2 Amb aquest mètode fem que el cardView s'oculte quan fem scroll
        recyclerView.setOnScrollChangeListener((v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            Animation animation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.ocultar);
            animation.start();
            cardView.setVisibility(View.GONE);
        });

        // set recyclerView with a linear layout manager
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL
                , false));

    }

    private void setListAdapter(){
        // MyContacts class gets data from ContactsProvider
        myContacts = new MyContacts(this);
        // set adapter to recyclerView
        recyclerView.setAdapter(new MyAdapter(myContacts, this));

        //TODO EX 10.1 Amb aquesta línea li afegim una línea divisoria entre cada element del recyclerview
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext()
                , DividerItemDecoration.VERTICAL));

        // Hide empty list TextView
        if (myContacts.getCount() > 0){
            findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);
        }
    }

    private boolean checkPermissions(){
        // check for permissions granted before setting listView Adapter data
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED){
            // opens Dialog: request user to grant permission
            ActivityCompat.requestPermissions(this, MainActivity.PERMISSIONS_CONTACTS
                    , MainActivity.REQUEST_CONTACTS);
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){

        if (requestCode == REQUEST_CONTACTS){
            // We have requested one READ permission for contacts, so only need [0] to be checked.
            if (grantResults[0] == PackageManager.PERMISSION_DENIED){
                setListAdapter();
            }else {
                Toast.makeText(this, getString(R.string.contacts_read_right_required)
                        , Toast.LENGTH_LONG).show();
            }
        }else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    //TODO EX 10.2 Implementem la funcionalitat de clickar els items
    @Override
    public void onItemClick(int position, Contact contact) {
        fillBaseCardView(contact, position);

        //TODO EX 10.2 Ací instanciem una animació per a quan aparega el cardView
        Animation animation= AnimationUtils.loadAnimation(this, R.anim.mostrar);
        cardView.setVisibility(View.VISIBLE);
        cardView.startAnimation(animation);
    }

    //TODO EX 10.2 Implementem la funcionalitat de mantindre polsat un ítem
    @Override
    public boolean onItemLongClick(int position, Contact contact) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contact.getContactId()));
        intent.setData(uri);
        startActivity(intent);
        cardView.setVisibility(View.GONE);
        return true;
    }

    //TODO EX 10.2 Amb aquest mètode emplenem el cardView que em creat per a mostrar totes les dades
    private void fillBaseCardView(Contact contact, int position){
        TextView baseContactName = findViewById(R.id.tvBaseContactName);
        TextView baseContactNumber = findViewById(R.id.tvBaseContactNumber);
        TextView baseContactType = findViewById(R.id.tvContactType);
        TextView baseId = findViewById(R.id.tvId);
        TextView baseContactId = findViewById(R.id.tvBaseContactIdText);
        TextView baseRawContact = findViewById(R.id.tvRawContact);
        TextView baseLookup = findViewById(R.id.tvLookup);

        baseContactName.setText(contact.getName());
        baseContactNumber.setText(contact.getNumber());
        baseContactType.setText(chooseType(contact.getType()));
        baseId.setText(String.valueOf(position));
        baseContactId.setText(contact.getContactId());
        baseRawContact.setText(contact.getRawContact());
        baseLookup.setText(contact.getLookup());
    }

    //TODO 10.2 EX Aquest mètode l'he creat per a que según el tipo de contacte mostre HOME,MOBILE,WORK o OTHER
    private String chooseType(String type){
        switch(type){
            case "1":
                return "HOME";
            case "2":
                return "MOBILE";
            case "3":
                return "WORK";
            default:
                return "OTHER";
        }
    }


}