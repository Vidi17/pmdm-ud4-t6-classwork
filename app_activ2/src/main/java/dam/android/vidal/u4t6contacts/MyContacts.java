package dam.android.vidal.u4t6contacts;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;

import dam.android.vidal.u4t6contacts.model.Contact;

public class MyContacts {
    private final ArrayList<Contact> myDataSet;
    private final Context context;

    public MyContacts(Context context) {
        this.context = context;
        this.myDataSet = getContacts();
    }

    // Get contacts list from ContactsProvider
    private ArrayList<Contact> getContacts() {
        ArrayList<Contact> contactsList = new ArrayList<>();

        //acces to ContentProvider
        ContentResolver contentResolver = context.getContentResolver();


        // aux variables
        String[] projection = new String[]{ContactsContract.Data._ID,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,

                //TODO 10.1 Aquí recogemos los demás datos que nos piden tales como el contactID etc
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.LOOKUP_KEY,
                ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.Data.PHOTO_THUMBNAIL_URI};

        String selectionFilter = ContactsContract.Data.MIMETYPE + "='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER + " IS NOT NULL";

        // query required data
        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                projection, selectionFilter, null,
                ContactsContract.Data.DISPLAY_NAME + " ASC");

        if (contactsCursor != null){
            // get the column indexes for desired Name and Number columns
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);

            //TODO 10.1 Aquí recogeremos todos los índices de cada una de las columnas que hemos almacenado
            int contactIdIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.CONTACT_ID);
            int lookupIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.LOOKUP_KEY);
            int rawContactIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int typeIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);


            // read data and add to Arraylist
            while (contactsCursor.moveToNext()){

                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                String contactId = contactsCursor.getString(contactIdIndex);
                String lookup = contactsCursor.getString(lookupIndex);
                String rawContact = contactsCursor.getString(rawContactIndex);
                String type = contactsCursor.getString(typeIndex);

                //TODO 10.1 Ara totes les dades de cada contacte les emmagatzem en un objecte Contact
                Contact contact = new Contact(contactId, name, number, lookup, rawContact, type
                        , getPhotoUriFromID(contactId));

                contactsList.add(contact);
            }
            contactsCursor.close();
        }

        return contactsList;
    }

    public ArrayList<Contact> getMyDataSet() {
        return myDataSet;
    }

    public int getCount(){
        return myDataSet.size();
    }

    private Uri getPhotoUriFromID(String id) {
        try {
            Cursor cur = context.getContentResolver()
                    .query(ContactsContract.Data.CONTENT_URI,
                            null,
                            ContactsContract.Data.CONTACT_ID
                                    + "="
                                    + id
                                    + " AND "
                                    + ContactsContract.Data.MIMETYPE
                                    + "='"
                                    + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                                    + "'", null, null);
            if (cur != null) {
                if (!cur.moveToFirst()) {
                    return null; // no photo
                }
            } else {
                return null; // error in cursor process
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Uri person = ContentUris.withAppendedId(
                ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id));
        return Uri.withAppendedPath(person,
                ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
    }
}
