package dam.android.vidal.u4t6contacts;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.android.vidal.u4t6contacts.model.Contact;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
    private final ArrayList<Contact> myContacts;

    //TODO EX 10.2 Creem una interface per a quan polsem un ítem i a més per quan el polsem de manera prolongada
    public interface OnClickListener{
        void onItemClick(int position, Contact contact);
        boolean onItemLongClick(int position, Contact contact);
    }

    private final OnClickListener listener;

    // Class for each item: contains only TextView
    static class MyViewHolder extends RecyclerView.ViewHolder{
        private final TextView tvContactId;
        private final TextView tvContactNumber;
        private final TextView tvContactName;
        private final ImageView ivContactPhoto;

        public MyViewHolder(View view){
            super(view);
            tvContactName = view.findViewById(R.id.tvContactName);
            tvContactId = view.findViewById(R.id.tvContactId);
            tvContactNumber = view.findViewById(R.id.tvContactNumber);
            ivContactPhoto = view.findViewById(R.id.ivContactPhoto);
        }

        // sets viewHolder views with data
        //TODO EX 10.1 Modifiquen alguns mètodes per a que ara funcionen en la clase Contact
        public void bind(Contact contact, OnClickListener listener){
            this.itemView.setOnClickListener(v -> listener.onItemClick(getAdapterPosition(), contact));
            this.itemView.setOnLongClickListener(v -> listener.onItemLongClick(getAdapterPosition(), contact));

            tvContactName.setText(contact.getName());
            tvContactId.setText(contact.getContactId());
            tvContactNumber.setText(contact.getNumber());

            if (contact.getPhotoThumbnail() != null){
                ivContactPhoto.setImageURI(contact.getPhotoThumbnail());
            }else {
                ivContactPhoto.setImageResource(R.drawable.ic_launcher_foreground);
            }
        }
    }

    // constructor: myContacts contains Contacts data
    MyAdapter(MyContacts myContacts, OnClickListener listener){
        this.myContacts = myContacts.getMyDataSet();
        this.listener = listener;

    }

    // Creates new view item: Layout Manager calls this method
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        // Create item view:
        // use a simple TextView predefined layout (sdk/platforms/android-xx/data/res/layout) that
        // contains only TextView
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        return new MyViewHolder(v);
    }

    //replaces the data content of a viewHolder (recycles old viewholder):
    // Layout Manager calls this method
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position){
        // bind viewHolder with data at: position
        viewHolder.bind(myContacts.get(position), listener);
    }

    // returns the size of dataset: Layout Manager calls this method
    @Override
    public int getItemCount(){
        return myContacts.size();
    }

}
